## Drone

#### Link

- https://afoo.me/posts/2015-12-17-create-a-gitlab-oauth2-application.html
- https://blog.51cto.com/tonegu/2497290 (**import**)
  

#### OAuth

- Gitlab OAuth Application

![Screenshot from 2021-09-03 22-49-52](./Screenshot from 2021-09-03 22-49-52.png)

```shell script
Application ID: 6ec4c92dc28e0e89b4e2e01a8e9c531af42260796be4c8f17be2a99348be09a9
Secret: 2646eead5fe9e14fc469e63301313de3468f1d687d0e7bff0a8257c3c3d29e63
Callback URL: http://106.75.53.230/login
```

- Rand Password

```shell script
$ openssl rand -hex 16
91e4ff8983b0829b87d31f8a799a35d2
```

#### Install

- 部署Drone-sever

```
docker run \
  --volume=/var/run/docker.sock:/var/run/docker.sock \
  --volume=/var/lib/drone:/data \
  --env=DRONE_LOGS_DEBUG=true \
  --env=DRONE_GIT_ALWAYS_AUTH=false \
  --env=DRONE_GITLAB_SERVER=http://39.104.88.120 \
  --env=DRONE_GITLAB_CLIENT_ID=d6272993ac02c3bb4069d73bf0ff8dabeaff47c0739ae27d1a23e8b80e33faa5 \
  --env=DRONE_GITLAB_CLIENT_SECRET=01f454fe0a55256a974d420b8ca023df6efc80b33d8a917dd16138b152b73253 \
  --env=DRONE_RPC_SECRET=12345678\
  --env=DRONE_RUNNER_CAPACITY=3 \
  --env=DRONE_SERVER_HOST=39.104.93.96\
  --env=DRONE_SERVER_PROTO=http \
  --env=DRONE_TLS_AUTOCERT=false \
  --publish=80:80 \
  --publish=443:443 \
  --restart=always \
  --detach=true \
  --name=drone  \
  drone/drone:latest
```

参数说明：

![60b0f159f5adb223d4cc325dfef8f590](./60b0f159f5adb223d4cc325dfef8f590.png)

- 部署Drone-runner

```
  docker run -d \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e DRONE_RPC_PROTO=http \
  -e DRONE_RPC_HOST=172.16.0.94 \
  -e DRONE_RPC_SECRET=12345678 \
  -e DRONE_RUNNER_CAPACITY=3 \
  -e DRONE_RUNNER_NAME=${HOSTNAME} \
  -p 3000:3000 \
  --restart always \
  --name runner \
  drone/drone-runner-docker:latest
```

![7db73189c5cb942bc812c483969c35b3](./7db73189c5cb942bc812c483969c35b3.png)

- 访问

[http://106.75.53.230](http://106.75.53.230 )

-  创建.drone.yml (在激活)

```bigquery
$ touch .drone.yml
```

